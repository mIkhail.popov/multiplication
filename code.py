try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
import cv2
import time
import os
from PIL import ImageGrab
import keyboard

x_pad = 34
y_pad = 21


def screen_grab():
    box = (35, 22, 75, 40)
    im = ImageGrab.grab(box)
    im.save(os.getcwd() + '\\full_snap__.png', 'PNG')

pytesseract.pytesseract.tesseract_cmd = r'C:\Users\rokot\AppData\Local\Tesseract-OCR\tesseract.exe'


def get_str():
    screen_grab()

    str = pytesseract.image_to_string(Image.open('full_snap__.png'))
    return str

def calculate_str(str):
    if '+' in str:
        plus = str.index('+')
        a = str[:plus]
        b = str[plus + 1:]
        result = int(a) + int(b)
    elif '*' in str:
        multiply = str.index('*')

        a = str[:multiply]
        b = str[multiply + 1:]
        result = int(a)*int(b)
    elif '-' in str:
        minus = str.index('-')
        a = str[:minus]
        b = str[minus + 1:]
        result = int(a) - int(b)
    elif '/' in str:
        divide = str.index('/')
        a = str[:divide]
        b = str[divide + 1:]
        result = int(a) / int(b)
    else:
        return None
    return int(result)


def print_answer(answer):
    keyboard.write(str(answer))
    keyboard.press('enter')


def main():
    time.sleep(10)
    while True:
        time.sleep(1)
        try:
            str = get_str()
            answer = calculate_str(str=str)
            print_answer(answer)
            if keyboard.is_pressed('q'):  # if key 'q' is pressed
                print('ABORT MISSION!!')
                break  # finishing the loop
        except ValueError as e:
            print(e)
            pass


main()
