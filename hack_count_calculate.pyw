import pyautogui
import cv2
import time
from PIL import ImageGrab
import os
import win32api
import win32con

x_pad = 86
y_pad = 112


def screenGrab():
    box = (x_pad+1, y_pad+1, x_pad+220, y_pad+75)
    im = ImageGrab.grab(box)
    im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) +
            '.png', 'PNG')


# def mousePos(cord):
#     win32api.SetCursorPos((x_pad + cord[0], y_pad + cord[1])


def get_cords():
    x, y = win32api.GetCursorPos()
    x = x - x_pad
    y = y - y_pad
    print(x, y)

def main():
    screenGrab()


if __name__ == '__main__':
    main()
