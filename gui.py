from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox
import multiplication
import random
import time as tm


calculation_mode = '+'
seconds_to_solve = 20
score = 0
number_1 = 2
number_2 = 2
reward_value = 1
punish_value = 10
error_list = []
window = Tk()
window.title("Multiplication")
window.geometry('540x300')
txt = Entry(window, width=20)
txt.grid(column=1, row=1)
txt.focus()

combo_lbl = Label(window, text='set multiplication range: ')
combo_lbl.grid(column=1, row=8)
combo1 = Combobox(window)
combo1['values'] = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
combo1.current(0)
combo1.grid(column=1, row=12)
combo2 = Combobox(window)
combo2['values'] = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
combo2.current(0)
combo2.grid(column=1, row=14)
mode_text = Label(window, text='Mode')
mode_text.grid(column=5, row=5)
mode = Combobox(window)
mode['values'] = ('+', '-', '*', '/')
mode.current(2)
mode.grid(column=5, row=6)


def get_task():
    global number_1
    global number_2
    global calculation_mode
    ordinary_pair = multiplication.multiplication(low_range=combo1.get(), high_range=combo2.get())
    if not len(error_list) == 0:
        error_pair = error_list[random.randint(a=0, b=len(error_list) - 1)]
    else:
        error_pair = ordinary_pair

    if random.random() < 0.75:
        number_1 = ordinary_pair[0]
        number_2 = ordinary_pair[1]
    else:
        number_1 = error_pair[0]
        number_2 = error_pair[1]
    task = Label(window, text=str(number_1) + ' ' + calculation_mode + ' ' + str(number_2) + '  = ?')
    task.grid(column=1, row=0)


points = Label(window, text='points: ' + str(score))
points.grid(column=2, row=0)
result = Label(window, text='correct')
result.grid(column=1, row=2)
get_task()


def reward():
    global score
    global reward_value
    score += reward_value
    points.configure(text='points: ' + str(score))


def punish():
    global score
    global punish_value
    score -= punish_value
    points.configure(text='points: ' + str(score))


def refresh_mode():
    global calculation_mode
    calculation_mode = mode.get()


def run_timer():
    global time_check


def check():
    global number_1
    global number_2
    global calculation_mode
    global time_check
    answer = txt.get()

    if calculation_mode == '*':
        correct_answer = str(number_1 * number_2)
    elif calculation_mode == '+':
        correct_answer = str(number_1 + number_2)
    elif calculation_mode == '-':
        correct_answer = str(number_1 - number_2)
    elif calculation_mode == '/':
        correct_answer = str(number_1 // number_2)
    else:
        pass
    if answer == correct_answer:
        result.configure(text='correct')
        reward()
    else:
        result.configure(text='not correct')
        error = [number_1, number_2]
        if error not in error_list:
            error_list.append(error)
        print(error_list)
        punish()


def clear_txt():
    global txt
    txt.delete(0, END)


def refresh_values():
    global score
    global reward_value
    global punish_value
    global seconds_to_solve
    if score > 100:
        punish_value = int(score * 0.10)
    seconds_to_solve = int(20 - (score * 0.1))
    time.configure(text='Time to solve: ' + str(seconds_to_solve) + ' seconds')


def times_up():

    messagebox.showinfo('Time is up', 'Too slow, fucker')
    main()


time = Label(window, text='Time to solve: ' + str(seconds_to_solve) + ' seconds')
time.grid(column=6, row=15)


# def tick():
#     global sec
#     time['text'] = sec
#     print(sec)
#     # if sec <= 0:
#     #     times_up()
#     #     return None
#     # sec -= 1
#     time.after(20000, times_up)


def main(event=NONE):
    global seconds_to_solve
    global time_check
    sec = 20
    try:
        window.after_cancel(time_check)
    except Exception as e:
        print(e)
        pass
    check()
    refresh_mode()
    get_task()
    clear_txt()
    refresh_values()
    time_check = window.after(seconds_to_solve * 1000, times_up)


window.bind('<Return>', main)
btn = Button(window, text="Next", command=main)
btn.grid(column=2, row=2)
window.mainloop()
